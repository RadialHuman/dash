# https://www.youtube.com/watch?v=J_Cy_QjG6NE
# https://pythonprogramming.net/data-visualization-application-dash-python-tutorial-introduction/
# server : 127.0.0.1:8050

# user can use the app to get the data and make the dashboard

import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
df = pd.read_csv("E:\\etc\\iris.csv")

app = dash.Dash()
app.layout = html.Div(children = [
	html.H1("Dash try_out_1"),
	dcc.Graph(id = "sample",
				figure = {
				"data":[
				{"x":df.index, "y": list(df.a), "type": "line", "name": df.columns[0]},
				{"x":df.index, "y": list(df.b), "type": "bar", "name": df.columns[1]}
				],
				"layout": {
					"title": "Sample dashboard"
				}
				})
	])


# app = dash.Dash()
# app.layout = html.Div(children = [
# 	dcc.Input(id = 'input', value = '-', type = "text"), # to get input from one place
# 	html.Div(id = "output") # to throw output to some other place with id
# 	])

# # wrapper ahead
# @app.callback(
# 	Output(component_id = "output", component_property = "children"),# can be used to modify the layout
# 	[Input(component_id = "input", component_property = "value")]) 
# # function to use this layout
# def wrapper_update_value(input_data):
# 	try:
# 		#return("Input:{}".format(input_data)) # returns anything you enter
# 		return(str(float(input_data)**2)) # returns square of number entered
# 	except:
# 		return("Enter a number")

if __name__ == "__main__":
    app.run_server(debug=True)