# https://www.youtube.com/watch?v=J_Cy_QjG6NE
# https://pythonprogramming.net/data-visualization-application-dash-python-tutorial-introduction/
# server : 127.0.0.1:8050

import dash
import dash_core_components as dcc
import dash_html_components as html

app = dash.Dash()
app.layout = html.Div(children = [
	html.H1("Dash try_out_1"),
	dcc.Graph(id = "sample",
				figure = {
				"data":[
				{"x":[1,2,3,4,5],"y": [7,3,1,9,6],"type": "line","name": "title"},
				{"x":[1,2,3,4,5],"y": [9,6,2,6,4],"type": "bar","name": "title2"}
				],
				"layout": {
					"title": "Sample dashboard"
				}
				})
	])

if __name__ == "__main__":
    app.run_server(debug=True)